import React                              from 'react';
import { render }                         from 'react-dom';
import { object }                         from 'prop-types';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ApolloProvider }                 from 'react-apollo';

import QuizListContainer from '../quiz/QuizListContainer';
import QuizContainer     from '../quiz/QuizContainer';
import HistoryContainer  from '../history/HistoryContainer';

// Constants
// ---------------------------------------------------------
const win        = typeof window === 'undefined' ? {} : window;
const warn       = console.warn.bind(console);
const APP_DOM_ID = 'App';
const MSG        = {
    NO_DOM_FOUND: 'Cannot find App DOM, failed to render.',
};

ApolloProvider.displayName = 'ApolloProvider';

// App Component
// ---------------------------------------------------------
const App = ({ client }) => (
    <ApolloProvider client={client}>
        <Router>
            <div className="app-container">
                <h2 className="p-playground__heading">Apollo app demo</h2>

                <Route exact path="/" component={QuizListContainer} />
                <Route path="/quiz/:id" component={QuizContainer} />
                <Route path="/history/:id" component={HistoryContainer} />
            </div>
        </Router>
    </ApolloProvider>
);

App.propTypes = {
    client: object.isRequired, // eslint-disable-line react/forbid-prop-types
};

App.init = ({ client }, w = win) => {
    if (!w.document) {
        return;
    }
    const appDOM = w.document.getElementById(APP_DOM_ID);

    if (!appDOM) {
        warn(MSG.NO_DOM_FOUND);
        return;
    }

    render(<App client={client} />, document.getElementById('App'));
};

App.displayName = 'App';

export default App;
