import React             from 'react';
import { shape, string } from 'prop-types';
import gql               from 'graphql-tag';
import { Query }         from 'react-apollo';
import { Link }          from 'react-router-dom';
import {
    get, set, map, compose, find, curry, assign,
}                        from 'lodash/fp';
import getSet            from '../util/getSet';
import QuizForm          from './QuizForm';
import Loading           from '../placeholder/Loading';
import Error             from '../placeholder/Error';

const QUERY_SINGLE_QUIZ = gql`
    query SingleQuiz($id: ID!) {
        quiz(id: $id) {
            id
            title
            description
            question {
                id
                category
                description
                option {
                    id
                    key
                    value
                }
                answer {
                    id
                    key
                    value
                }
            }
            best @client
        }
        currentTest @client {
            id @client
            yourAnswer @client
        }
    }
`;

// helpers
// ---------------------------------
const isChecked = (id, selected = []) => typeof find({ id }, selected) !== 'undefined';

const checkOption = curry((selected, option) => map(
    optItem => set('checked', isChecked(optItem.id, selected), optItem),
    option,
));

const mapTestToQuestions = curry((currentTest, questions) => map(
    (queItem) => {
        const selected = compose(
            get('selected'),
            find({ questionId: queItem.id }),
        )(currentTest);

        return getSet(
            'option',
            'option',
            checkOption(selected),
            queItem,
        );
    },
    questions,
));

const mapTestToQuiz = (currentTest, quiz) =>
    getSet('question', 'question', mapTestToQuestions(currentTest), quiz);


const Quiz = ({ match: { params } }) => {
    const { id } = params || {};
    return (
        <Query query={QUERY_SINGLE_QUIZ} variables={{ id }} displayName="SingleQuizQuery">
            {
                ({ loading, error, data: { quiz, currentTest } }) => {
                    if (loading) {
                        return (<Loading />);
                    }
                    if (error) {
                        return (<Error error={error} />);
                    }

                    const mappedQuiz = mapTestToQuiz(currentTest.yourAnswer, quiz);

                    return (
                        <div>
                            <Link className="c-btn" to="/">Back to quiz list</Link>
                            <QuizForm quiz={mappedQuiz} />
                        </div>
                    );
                }
            }
        </Query>
    );
};

Quiz.propTypes = {
    match: shape({
        params: shape({
            id: string.isRequired,
        }),
    }).isRequired,
};

Quiz.defaultValue = {
    description: '',
};

export default Quiz;
