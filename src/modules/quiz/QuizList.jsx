import React                              from 'react';
import { string, arrayOf, shape, number } from 'prop-types';
import { Link }                           from 'react-router-dom';

// Sub component
// ---------------------------------
const QuizItem = ({ id, title, description, best, question }) => {
    return (
        <li className="c-quiz__item" key={id}>
            <div className="c-quiz__item-inner">
                <div className="c-quiz__item-col title">
                    <h4>{title}</h4>
                </div>
                <div className="c-quiz__item-col desc">
                    <p>{description}</p>
                </div>
                <div className="c-quiz__item-col best">
                    {best} / {question.length}
                </div>
                <div className="c-quiz__item-col cta">
                    <Link className="c-btn u-uppercase" to={`/quiz/${id}`}>start</Link>
                    <br />
                    <Link className="c-btn u-uppercase" to={`/history/${id}`}>history</Link>
                </div>
            </div>
        </li>
    );
};

QuizItem.propTypes = {
    id:          string.isRequired,
    title:       string.isRequired,
    description: string,
    question:    arrayOf(shape({
        id: string.isRequired,
    })),
    best:        number,
};

QuizItem.defaultProps = {
    description: '',
    question:    [],
    best:        0,
};


// Main component
// ---------------------------------
const QuizList = ({ quizzes }) => (
    <div>
        <div className="c-quiz__header">
            <div className="c-quiz__item-inner">
                <div className="c-quiz__item-col title">Title</div>
                <div className="c-quiz__item-col desc">Description</div>
                <div className="c-quiz__item-col best">Best score</div>
                <div className="c-quiz__item-col cta" />
            </div>
        </div>

        <ul className="c-quiz__list">
            {quizzes.map((quiz) => {
                const key = `quiz-${quiz.id}`; // unique DOM ID
                return (<QuizItem key={key} {...quiz} />);
            })}
        </ul>
    </div>
);

QuizList.propTypes = {
    quizzes: arrayOf(shape({
        id: string.isRequired,
    })),
};

QuizList.defaultProps = {
    quizzes: [],
};

QuizList.displayName = 'QuizList';

export default QuizList;
