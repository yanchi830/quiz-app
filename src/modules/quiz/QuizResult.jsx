import React                            from 'react';
import { moment }                       from 'moment';
import { shape, string, arrayOf, bool } from 'prop-types';
import { map, compose, join }           from 'lodash/fp';

// Constants
// --------------------------------------------------
const PROP_TYPE_OPTION = {
    id:    string.isRequired,
    key:   string.isRequired,
    value: string.isRequired,
};

// Unit functions
// --------------------------------------------------
const formatKey = optionArray => compose(
    join(', '),
    map(option => option.key.toUpperCase()),
)(optionArray);


// Sub components
// --------------------------------------------------
const OptionItem = ({ option }) =>
    (<li>{option.key.toUpperCase()}: {option.value}</li>);

OptionItem.propTypes = {
    option: shape(PROP_TYPE_OPTION).isRequired,
};

export const ResultItem = ({ description, isCorrect, option, answer, yourAnswer }) => {
    return (
        <li className="c-quiz__result-item">
            <p>Question: {description}</p>
            <ul>
                {map(eachOpt => <OptionItem key={eachOpt.id} option={eachOpt} />, option)}
            </ul>
            <p>Selected: {formatKey(yourAnswer)}</p>
            <p>
                {
                    isCorrect ? <span className="u-correct">Correct!</span> : (
                        <span>
                            <span className="u-wrong">Wrong!</span>
                            &nbsp;Correct answer: {formatKey(answer)}
                        </span>
                    )
                }
            </p>
        </li>
    );
};

ResultItem.propTypes = {
    description: string,
    isCorrect:   bool.isRequired,
    option:      arrayOf(shape(PROP_TYPE_OPTION)),
    yourAnswer:  arrayOf(shape(PROP_TYPE_OPTION)),
    answer:      arrayOf(shape(PROP_TYPE_OPTION)),
};

ResultItem.defaultProps = {
    description: '',
    option:      [],
    yourAnswer:  [],
    answer:      [],
};

// Default component
// --------------------------------------------------
const QuizResult = ({ quiz }) => {
    const { title, description, question } = quiz;
    return (
        <div className="c-quiz__result">
            <h4>Your Result of <strong>{title}</strong>:</h4>
            <i>{description}</i>
            <ul className="c-quiz__result-list">
                {map(
                    eachQuestion => <ResultItem key={eachQuestion.id} {...eachQuestion} />,
                    question,
                )}
            </ul>
        </div>
    );
};

QuizResult.propTypes = {
    quiz: shape({
        id:          string.isRequired,
        quizId:      string.isRequired,
        timestamp:   String.isRequired,
        title:       string,
        description: string,
        question:    arrayOf(shape({
            id:          string.isRequired,
            description: string.isRequired,
            category:    string,
            isCorrect:   bool,
            option:      arrayOf(shape(PROP_TYPE_OPTION)),
            answer:      arrayOf(shape(PROP_TYPE_OPTION)),
            yourAnswer:  arrayOf(shape(PROP_TYPE_OPTION)),
        })),
    }),
};

QuizResult.defaultProps = {
    quiz: {},
};

export default QuizResult;
