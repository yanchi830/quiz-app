import React     from 'react';
import { Query } from 'react-apollo';
import gql       from 'graphql-tag';
import QuizList  from './QuizList';
import Loading   from '../placeholder/Loading';
import Error     from '../placeholder/Error';

// query
// ---------------------------------
const QUERY_QUIZZES = gql`
    query Quizzes {
        quizzes {
            id
            title
            description
            question {
                id
                category
                description
                option {
                    id
                    key
                    value
                }
            }
            best @client
        }
    }
`;

// component
// ---------------------------------
const QuizContainer = () => (
    <Query query={QUERY_QUIZZES} displayName="QuizListQuery">
        {({ loading, error, data: { quizzes } }) => {
            if (loading) return (<Loading />);
            if (error) return (<Error error={error} />);

            return (
                <QuizList quizzes={quizzes} />
            );
        }}
    </Query>
);

export default QuizContainer;
