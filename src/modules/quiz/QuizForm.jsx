import React                      from 'react';
import { string, shape, arrayOf } from 'prop-types';
import { Mutation }               from 'react-apollo';
import get                        from 'lodash/fp/get';
import gql                        from 'graphql-tag';
import QuestionList               from '../question/QuestionList';
import Loading                    from '../placeholder/Loading';
import Error                      from '../placeholder/Error';
import QuizResult                 from './QuizResult';

// Constants
// ------------------------------------------
const SUBMIT_QUIZ = gql`
    mutation SubmitQuiz($input: Quiz) {
        submitQuiz(input: $input) @client
    }
`;

const QuizForm = ({ quiz }) => {
    const { id, title, description, question } = quiz;

    return (
        <Mutation mutation={SUBMIT_QUIZ} variables={{ input: quiz }}>
            {(submitQuiz, { called, loading, data, error }) => {
                if (loading) return (<Loading />);
                if (error) return (<Error error={error} />);
                if (called) return (<QuizResult quiz={get('submitQuiz', data)} />);

                const formId = `quiz-${id}`;

                const _onSubmit = (e) => {
                    e.preventDefault();
                    submitQuiz(e);
                };

                return (
                    <form id={formId} onSubmit={_onSubmit}>
                        <h3 className="c-quiz__heading">{title}</h3>
                        <p className="c-quiz__description">{description}</p>

                        <QuestionList question={question} quizId={id} />

                        <div className="c-quiz__button">
                            <button className="c-quiz__submit" type="submit">Submit</button>
                        </div>
                    </form>
                );
            }}
        </Mutation>
    );
};

QuizForm.propTypes = {
    quiz: shape({
        id:          string.isRequired,
        title:       string.isRequired,
        description: string,
        question:    arrayOf(shape({
            id: string.isRequired,
        })),
        answer:      arrayOf(shape({
            id: string.isRequired,
        })),
    }),
};

QuizForm.defaultProps = {
    quiz: {},
};

export default QuizForm;
