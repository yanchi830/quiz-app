import React                      from 'react';
import { arrayOf, shape, string } from 'prop-types';
import set                        from 'lodash/fp/set';
import Option                     from './Option';

// Default Component
// ---------------------------------------------
const OptionList = ({ option, quizId, questionId }) => (
    <ol className="c-opt__list">
        {option.map((opt) => {
            const key = `quiz-${quizId}_question-${questionId}_option-${opt.id}`;
            const optProps = set('k', opt.key, opt);

            return (<Option {...optProps} key={key} inputDOMId={key} quizId={quizId} questionId={questionId} />);
        })}
    </ol>
);

OptionList.propTypes = {
    quizId:     string.isRequired,
    questionId: string.isRequired,
    option:     arrayOf(shape({
        id: string.isRequired,
    })),
};

OptionList.defaultProps = {
    option: [],
};

OptionList.displayName = 'OptionList';

export default OptionList;
