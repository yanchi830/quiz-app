import React            from 'react';
import { string, bool } from 'prop-types';
import { Mutation }     from 'react-apollo';
import gql              from 'graphql-tag';

const CHECK_OPTION = gql`
    mutation checkOption($input: Option!, $quizId: ID!, $questionId: ID!, $checked: Boolean!) {
        checkOption(input: $input, quizId: $quizId, questionId: $questionId, checked: $checked) @client
    }
`;

const onError = (err) => {
    console.log(err);
};

// Default Component
// ---------------------------------------------
const Option = ({ inputDOMId, quizId, questionId, id, k, value, checked }) => {
    // build input for mutation
    const option = {
        id,
        key: k,
        value,
        __typename: 'Option',
    };

    return (
        <Mutation
            mutation={CHECK_OPTION}
            variables={{ input: option, questionId, checked: !checked }}
            onError={onError}
        >
            {(checkOption) => {
                const name = `quiz-${quizId}_question-${questionId}`;
                return (
                    <li className="c-opt__item">
                        <input
                            onClick={checkOption}
                            checked={checked}
                            id={inputDOMId}
                            name={name}
                            type="checkbox"
                            className="c-opt__input"
                            value={k}
                        />
                        <label className="c-opt__label" htmlFor={inputDOMId}>{value}</label>
                    </li>
                );
            }}
        </Mutation>
    );
}

Option.propTypes = {
    id:         string.isRequired,
    quizId:     string.isRequired,
    questionId: string.isRequired,
    inputDOMId: string.isRequired,
    k:          string.isRequired,
    value:      string.isRequired,
    checked:    bool.isRequired,
};

Option.displayName = 'Option';

export default Option;
