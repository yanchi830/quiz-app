import React               from 'react';
import { string, element } from 'prop-types';

const NoResult = ({ message, children }) => (
    <p>
        {message}
        {children}
    </p>
);

NoResult.propTypes = {
    message:  string,
    children: element,
};

NoResult.defaultProps = {
    message:  '',
    children: null,
};

export default NoResult;
