import React             from 'react';
import { shape, string } from 'prop-types';

const Error = ({ error }) => (
    <div>{ JSON.stringify(error) }</div>
);

Error.propTypes = {
    error: shape({
        message: string,
    }),
};

Error.defaultProps = {
    error: {},
};

export default Error;
