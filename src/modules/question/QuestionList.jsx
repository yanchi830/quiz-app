import React                      from 'react';
import { shape, arrayOf, string } from 'prop-types';
import Question                   from './Question';

const QuestionList = ({ question, quizId }) => (
    <ol className="c-ques__list">
        {question.map((ques) => {
            const key = `quiz-${quizId}_question-${ques.id}`;

            return (<Question {...ques} key={key} quizId={quizId} />);
        })}
    </ol>
);

QuestionList.propTypes = {
    question: arrayOf(shape({
        id: string.isRequired,
    })),
    quizId:   string.isRequired,
};

QuestionList.defaultProps = {
    question: [],
};

export default QuestionList;
