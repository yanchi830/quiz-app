import React                      from 'react';
import { string, arrayOf, shape } from 'prop-types';
import OptionList                 from '../option/OptionList';

const Question = ({ id, category, description, option, quizId }) => (
    <li className="c-ques__item">
        <span>{category}</span>
        <p className="c-ques__description"><strong>{description}</strong></p>
        <OptionList option={option} quizId={quizId} questionId={id} />
    </li>
);


Question.propTypes = {
    quizId:      string.isRequired,
    id:          string.isRequired,
    category:    string.isRequired,
    description: string.isRequired,
    option:      arrayOf(shape({
        id: string.isRequired,
    })),
};

Question.defaultProps = {
    option: [],
};

export default Question;
