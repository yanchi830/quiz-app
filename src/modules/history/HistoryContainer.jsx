import React             from 'react';
import { shape, string } from 'prop-types';
import { Query }         from 'react-apollo';
import { Link }          from 'react-router-dom';
import filter            from 'lodash/fp/filter';
import History           from './History';
import Loading           from '../placeholder/Loading';
import Error             from '../placeholder/Error';
import NoResult          from '../placeholder/NoResult';
import { QUERY_RESULT }  from '../../js/resolvers/mutation/submitQuiz';

// Constants
// ------------------------------
const MSG_NO_RESULT = 'No history found. Back to the quiz list to start a quiz.';


// Unit functions
// ------------------------------
/**
 * Filter results by quiz id
 *
 * @param id
 * @param results
 * @return {*}
 */
const filterResults = (id, results) => filter(result => result.quizId === id, results);

// Default component
// ------------------------------
const HistoryContainer = ({ match: { params } }) => {
    const { id } = params;

    return (
        <div className="c-quiz__history">
            <Link className="c-btn" to="/">Back to quiz list</Link>
            <Query query={QUERY_RESULT}>
                {
                    ({ loading, error, data: { results } }) => {
                        if (loading) return <Loading />;
                        if (error) return <Error error={error} />;
                        if (!results.length) return <NoResult message={MSG_NO_RESULT} />;

                        const filteredResults = filterResults(id, results);

                        return (<History results={filteredResults} />);
                    }
                }
            </Query>
        </div>
    );
};

HistoryContainer.propTypes = {
    match: shape({
        params: shape({
            id: string.isRequired,
        }),
    }),
};

HistoryContainer.defaultProps = {
    match: {},
};

export default HistoryContainer;
