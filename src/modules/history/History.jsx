import React                            from 'react';
import map                              from 'lodash/fp/map';
import filter                           from 'lodash/fp/filter';
import moment                           from 'moment';
import { arrayOf, shape, string, bool } from 'prop-types';
import NoResult                         from '../placeholder/NoResult';
import { ResultItem }                   from '../quiz/QuizResult';

// Constants
// ---------------------------------------
const PROP_TYPE_OPTION = {
    id:    string.isRequired,
    key:   string.isRequired,
    value: string.isRequired,
};

const MSG_NO_WRONG = 'Congrats! All answers are correct :D';

const MSG_NO_CORRECT = 'Ahh ... no correct answers ... more tests might help ;)';

// Sub component
// ---------------------------------------

const QuestionList = ({ question, noResultMessage }) => {
    if (question.length === 0) return <NoResult message={noResultMessage} />;

    return (
        <ul>
            {map(queItem => <ResultItem {...queItem} />, question)}
        </ul>
    );
};

const HistoryItem = ({ timestamp, title, description, question }) => {
    const corrects = filter(queItem => queItem.isCorrect, question);
    const wrongs   = filter(queItem => !queItem.isCorrect, question);

    return (
        <li className="c-quiz__history-item">
            <h4>
                {title}&nbsp;
                (
                <time dateTime={timestamp}>
                    {moment.unix(JSON.parse(timestamp)).format('MMMM Do YYYY, h:mm:ss a')}
                </time>
                )
            </h4>
            <i>{description}</i>

            <h5>Correct questions:</h5>
            <QuestionList question={corrects} noResultMessage={MSG_NO_CORRECT} />

            <h5>Wrong question:</h5>
            <QuestionList question={wrongs} noResultMessage={MSG_NO_WRONG} />
        </li>
    );
};

HistoryItem.propTypes = {
    id:          string.isRequired,
    quizId:      string.isRequired,
    timestamp:   string.isRequired,
    title:       string.isRequired,
    description: string,
    question:    arrayOf(shape({
        id:          string.isRequired,
        isCorrect:   bool,
        description: string,
        category:    string,
        option:      arrayOf(shape(PROP_TYPE_OPTION)),
        answer:      arrayOf(shape(PROP_TYPE_OPTION)),
        yourAnswer:  arrayOf(shape(PROP_TYPE_OPTION)),
    })),
};

HistoryItem.defaultProps = {
    description: '',
    question:    [],
};

// Default component
// ---------------------------------------
const History = ({ results }) => {
    return (
        <ul className="c-quiz__history-list">
            {map(
                result => <HistoryItem key={result.id} {...result} />,
                results,
            )}
        </ul>
    );
};

History.propTypes = {
    results: arrayOf(shape({
        id:          string.isRequired,
        quizId:      string.isRequired,
        timestamp:   string.isRequired,
        title:       string.isRequired,
        description: string,
        question:    arrayOf(shape({
            id:          string.isRequired,
            isCorrect:   bool,
            description: string,
            category:    string,
            option:      arrayOf(shape(PROP_TYPE_OPTION)),
            answer:      arrayOf(shape(PROP_TYPE_OPTION)),
            yourAnswer:  arrayOf(shape(PROP_TYPE_OPTION)),
        })),
    })),
};

History.defaultProps = {
    results: [],
};

export default History;
