/**
 * @file    getSet (lodash)
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    May 2018
 */

import { get, set, compose, curry, __ } from 'lodash/fp';

const getSet = curry((from, to, fn, obj) => compose(
    set(to, __, obj),
    fn,
    get(from),
)(obj));

export default getSet;
