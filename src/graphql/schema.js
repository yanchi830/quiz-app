const { readFileSync }         = require('fs');
const { resolve }              = require('path');
const { makeExecutableSchema } = require('graphql-tools');
const { filter }               = require('lodash/fp');

// fake data
const options = JSON.parse(readFileSync(resolve(__dirname, 'files', 'mockOptions.json'), 'utf8'));
const questions = JSON.parse(readFileSync(resolve(__dirname, 'files', 'mockQuestions.json'), 'utf8'));
const quizzes = JSON.parse(readFileSync(resolve(__dirname, 'files', 'mockQuizzes.json'), 'utf8'));

// graphql types
const catSchema = `
    enum Category {
        MATH
        GEOGRAPHY
        ENGLISH
    }
`;

const optionSchema = `
    type Option {
        id: ID!
        key: String
        value: String
    }
`;

const questionSchema = `
    type Question {
        id: ID!
        category: Category
        description: String
        option: [Option]
        answer: [Option]
    }
`;

const quizSchema = `
    type Quiz {
        id: ID!
        title: String
        description: String
        question: [Question]
    }
`;


const querySchema = `
    type Query {
        options: [Option]
        quizzes(id: ID): [Quiz]
        questions: [Question]
        question: Question
        quiz(id: ID): Quiz
    }
`;

const typeDefs = `
    ${catSchema}
    ${optionSchema}
    ${questionSchema}
    ${quizSchema}
    ${querySchema}
`;

// resolvers
const resolvers = {
    Query: {
        options:   () => options,
        questions: () => questions,
        quizzes:   (_, { id }) => {
            if (!id) {
                return quizzes;
            }

            return filter(quiz => (JSON.parse(quiz.id) === JSON.parse(id)), quizzes);
        },
        quiz: (_, { id }) => {
            if (!id) {
                return quizzes;
            }

            return filter(quiz => (JSON.parse(quiz.id) === JSON.parse(id)), quizzes)[0];
        },
    },
};

const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

module.exports = schema;
