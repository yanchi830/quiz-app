import {
    concat, assign, map, xor, get, set, compose, filter, __,
}                    from 'lodash/fp';
import moment        from 'moment/moment';
import gql           from 'graphql-tag';
import DEFAULT_STATE from '../../defaultState';
import getSet        from '../../../modules/util/getSet';


// Constants
// ---------------------------------------------
const FRAG_TEST = gql`
    fragment CurrentTestFrag on CurrentTest {
        yourAnswer
    }
`;

export const QUERY_RESULT = gql`
    query GetResult {
        results @client {
            id
            title
            description
            timestamp
            quizId
            question
        }
    }
`;

const QUERY_QUIZ = gql`
    fragment QuizFrag on Quiz {
        best
    }
`;


// Unit Functions
// ---------------------------------
/**
 * Set isCorrect for single question to output
 *
 * @param singleQuestion
 * @param singleQuestion.yourAnswer
 * @param singleQuestion.answer
 * @return {{
 *      isCorrect: {bool}
 * }}
 */
const setCorrect = (singleQuestion) => {
    const { yourAnswer, answer } = singleQuestion;

    const selectedIds = map(sel => sel.id, yourAnswer);
    const correctIds  = map(opt => opt.id, answer);

    return assign(singleQuestion, {
        isCorrect: xor(selectedIds, correctIds).length === 0,
    });
};

/**
 * Set "yourAnswer" by options with "checked": true
 *
 * @param singleQuestion
 * @param singleQuestion.yourAnswer
 * @param singleQuestion.option
 *
 * @return {*}
 */
const setYourAnswer = singleQuestion => compose(
    set('yourAnswer', __, singleQuestion),
    filter(optItem => optItem.checked),
    get('option'),
)(singleQuestion);

/**
 * Transform __typename: Question to __typename: ResultQuestion
 *
 * @param question
 * @param question.__typename
 *
 * @return {{ __typename }}
 */
const transformTypeForQuestion = set('__typename', 'ResultQuestion');

/**
 * Update "question" field in submitted quiz
 *
 * @param quiz
 * @param quiz.question
 *
 * @return {{ question }}
 */
const updateQuestion = getSet(
    'question',
    'question',
    map(compose(transformTypeForQuestion, setCorrect, setYourAnswer)),
);

/**
 * Transform __typename: Quiz to __typename: Result, add timestamp,
 * and use timestamp as id
 *
 * @param quiz
 * @return {*}
 */
const transformType = (quiz) => {
    const timestamp = JSON.stringify(moment().unix());
    return assign(quiz, {
        id:         timestamp,
        __typename: 'Result',
        quizId:     quiz.id,
        timestamp,
    });
};

/**
 * After submitting a quiz, push "currentTest" to "results",
 * then reset "currentTest"
 *
 * @param _
 * @param variable
 * @param variable.id
 * @param context
 * @param context.cache
 * @param context.getCacheKey
 * @return {{ __typename, id, timestamp, quizId, yourAnswer }}
 */
export default (_, { input }, { cache, getCacheKey }) => {

    // create a result from current test
    const prevResults = cache.readQuery({ query: QUERY_RESULT });
    const newResult = compose(
        transformType,
        updateQuestion,
    )(input);

    const data = {
        ...prevResults,
        results: concat(prevResults.results, newResult),
    };
    cache.writeQuery({ query: QUERY_RESULT, data });

    // update quiz's best
    const quizFragId = getCacheKey({ __typename: 'Quiz', id: input.id });
    const quizFrag = cache.readFragment({ id: quizFragId, fragment: QUERY_QUIZ });

    const newBest = compose(
        corrects => corrects.length,
        filter(queItem => queItem.isCorrect),
        get('question'),
    )(newResult);

    if (newBest > quizFrag.best) {
        cache.writeFragment({ id: quizFragId, fragment: QUERY_QUIZ, data: { ...quizFrag, best: newBest } });
    }

    // reset test
    const testFragId = getCacheKey({ __typename: 'CurrentTest', id: '0' });
    cache.writeFragment({ id: testFragId, fragment: FRAG_TEST, data: DEFAULT_STATE.currentTest });

    return newResult;
};
