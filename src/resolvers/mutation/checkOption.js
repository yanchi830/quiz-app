import gql    from 'graphql-tag';
import {
    findIndex, concat, curry, filter, get, compose, assign, find, __,
}             from 'lodash/fp';
import getSet from '../../../modules/util/getSet';

// Unit functions
// --------------------------------------------
/**
 * Update selected option ids
 * e.g. ["0", "1"] => ["0"] with optionId: "1" and checked: false
 *
 * @param {object}      input
 * @param {string}      input.id
 * @param {string}      input.key
 * @param {string}      input.value
 * @param {boolean}     checked
 * @param {string[]}    selected
 *
 * @return {string[]}   new selected
 */
export const updateSelected = curry((input, checked, selected) => {
    const hasChecked = typeof find({ id: input.id }, selected) !== 'undefined';

    if (checked && !hasChecked) {
        return concat(input, selected);
    }

    if (!checked && hasChecked) {
        return filter(selectedOption => selectedOption.id !== input.id, selected);
    }

    return selected;
});

/**
 * After checking/un-checking an option,
 * update "yourAnswer" in "currentTest"
 * in the local cache
 *
 * @param _
 * @param variable
 * @param variable.input
 * @param variable.questionId
 * @param variable.checked
 * @param context
 * @param context.cache
 * @param context.getCacheKey
 * @return {null}
 */
export default (_, { input, questionId, checked }, { cache, getCacheKey }) => {
    // get prev currentTest
    const fragment = gql`
                fragment CurrentTestFrag on CurrentTest {
                    yourAnswer
                }
            `;

    const fragId    = getCacheKey({ __typename: 'CurrentTest', id: '0' });
    const prevState = cache.readFragment({ id: fragId, fragment });

    // no answer been made
    if (prevState.yourAnswer.length === 0) {
        const testNextState = assign(prevState, {
            yourAnswer: [{
                id:         questionId,
                __typename: 'YourAnswer',
                selected:   [input],
                questionId,
            }],
        });

        cache.writeFragment({ id: fragId, fragment, data: testNextState });
        return null;
    }

    // else if no answer to this question has been made
    const yourAnswerIndex = compose(
        findIndex({ id: questionId }),
        get('yourAnswer'),
    )(prevState);

    if (yourAnswerIndex === -1) {
        const testNextState = getSet(
            'yourAnswer',
            'yourAnswer',
            concat({
                id:         questionId,
                __typename: 'YourAnswer',
                selected:   [input],
                questionId,
            }),
            prevState,
        );

        cache.writeFragment({ id: fragId, fragment, data: testNextState });
        return null;
    }

    // else answer exists in prev state
    const pathToSelected = `yourAnswer.${yourAnswerIndex}.selected`;
    const testNextState  = getSet(pathToSelected, pathToSelected, updateSelected(input, checked), prevState);

    cache.writeFragment({ id: fragId, fragment, data: testNextState });
    return null;
};
