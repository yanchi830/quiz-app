/**
 * Default state of client data
 */
export default {
    currentTest: {
        __typename: 'CurrentTest',
        id:         '0',
        yourAnswer: [],
    },
    results: [],
};
