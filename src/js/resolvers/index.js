import checkOption from './mutation/checkOption';
import submitQuiz  from './mutation/submitQuiz';

const resolvers = {
    Quiz:     {
        best: () => 0,
    },
    Mutation: {
        checkOption,
        submitQuiz,
    },
};

export default resolvers;
