/**
 * @file    Client Entry
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    May 2018
 */

import ApolloClient from 'apollo-boost';
import App          from '../modules/app/App';
import resolvers    from './resolvers/index';
import defaultState from './defaultState';

const schema = `
    type YourAnswer {
        id: ID!
        questionId: String!
        selected: [Option]
        correct: [Option]
    }

    type CurrentTest {
        id: ID!
        quizId: String
        yourAnswer: [YourAnswer]
    }
    
    type ResultQuestion {
        id: ID!
        description: String,
        category: Category,
        answer: [Option]
        yourAnswer: [Option]
        option: [Option]
    }
    
    type Result {
        id:         ID!,
        timestamp:  String!,
        quizId:     ID,
        question: [ResultQuestion],
    }
`;

const client = new ApolloClient({
    clientState: {
        defaults: defaultState,
        resolvers,
        typeDefs: schema,
    },
});

App.init({ client });

export default client;
