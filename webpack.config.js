/**
 * @file    Webpack configuration
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    May 2018
 */

const path = require('path');
const get = require('lodash/fp/get');
const config = require('./config.json');

// App config
const src = get('source', config);
const des = get('destination', config);

module.exports = {
    entry: {
        main: path.resolve(__dirname, src, 'js', 'index.js'),
    },
    output: {
        path: path.resolve(__dirname, des, 'js'),
        filename: 'bundle.js',
    },
    module: {
        rules: [{
            test: /\.jsx$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
            },
        }],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json'],
    },
};
