/**
 * @file    Gulp entry
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    May 2018
 */

const gulp = require('gulp');
const { resolve } = require('path');
const sass = require('gulp-sass');
const config = require('./config.json');

// App config
const src = config.source;
const des = config.destination;

const baseSrc = resolve(__dirname, src);
const baseDes = resolve(__dirname, des);

gulp.task('html', () => gulp
    .src(resolve(baseSrc, 'html', 'index.html'))
    .pipe(gulp.dest(baseDes)));

gulp.task('css', () => gulp
    .src(resolve(baseSrc, 'css', '*.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(resolve(baseDes, 'css'))));

gulp.task('files', () => gulp
    .src(resolve(baseSrc, 'files', '**/*'))
    .pipe(gulp.dest(resolve(baseDes, 'files'))));

gulp.task('startWatch', () => {
    const watcherCallback = (info) => {
        console.log(`File ${info.path} was ${info.type}`);
    };

    [
        { task: 'css', folder: 'css', ext: '.scss' },
        { task: 'html', folder: 'html', ext: '.html' },
        { task: 'files', folder: 'files', ext: '' },
    ].forEach(({ task, folder, ext }) => {
        gulp.watch(resolve(baseSrc, folder, `**/*${ext}`), [task])
            .on('change', watcherCallback)
            .on('add', watcherCallback)
            .on('unlink', watcherCallback);
    });
});

gulp.task('build', ['html', 'css', 'files']);

gulp.task('watch', ['build', 'startWatch']);
