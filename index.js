/**
 * @file    EXPRESS entry
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    May 2018
 */

const express = require('express');
const bodyParser = require('body-parser');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const { resolve } = require('path');
const schema = require('./src/graphql/schema');

// config
const { destination: des } = require('./config.json');

const destBase = resolve(__dirname, des);
const PORT = process.env.PORT || 5000;

// Express server start
const app = express();

// Create graphQL service
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));


// Serve HTMLs
app.use(express.static(resolve(destBase)));

app.get('/', (req, res) => {
    res.sendFile(resolve(destBase, 'index.html'));
});

// Listen
app.listen(PORT, () => {
    console.log(`Listening on ${PORT}`);
});
